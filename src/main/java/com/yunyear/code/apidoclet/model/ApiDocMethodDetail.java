package com.yunyear.code.apidoclet.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lenovo on 2017/8/18.
 */
public class ApiDocMethodDetail implements Serializable{
    private String api;
    private String name;
    private String desc;
    private String group;
    private String groupName;
    private List<String> method;
    private List<String> history;
    private List<ApiDocParamDetail> req;
    private List<ApiDocParamDetail> resp;
    private List<ApiDocCodeDetail> codes;

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<String> getMethod() {
        return method;
    }

    public void setMethod(List<String> method) {
        this.method = method;
    }

    public List<String> getHistory() {
        return history;
    }

    public void setHistory(List<String> history) {
        this.history = history;
    }

    public List<ApiDocParamDetail> getReq() {
        return req;
    }

    public void setReq(List<ApiDocParamDetail> req) {
        this.req = req;
    }

    public List<ApiDocParamDetail> getResp() {
        return resp;
    }

    public void setResp(List<ApiDocParamDetail> resp) {
        this.resp = resp;
    }

    public List<ApiDocCodeDetail> getCodes() {
        return codes;
    }

    public void setCodes(List<ApiDocCodeDetail> codes) {
        this.codes = codes;
    }

    @Override
    public String toString() {
        return "ApiDocDetail{" +
                "api='" + api + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", group='" + group + '\'' +
                ", groupName='" + groupName + '\'' +
                ", method=" + method +
                ", history=" + history +
                ", req=" + req +
                ", resp=" + resp +
                ", codes=" + codes +
                '}';
    }
}
