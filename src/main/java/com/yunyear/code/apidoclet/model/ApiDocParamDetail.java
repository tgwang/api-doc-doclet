package com.yunyear.code.apidoclet.model;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/8/18.
 */
public class ApiDocParamDetail implements Serializable {
    private transient ApiType apiType;  // apiType 类型
    private String param;     // 参数名称
    private String type;      // 参数类型
    private String range;    // 参数长度范围
    private String require;    // 参数似乎否必填
    private String desc;      // 参数描述
    private String opTime;      // 增加时间
    private String opName;      // 增加人姓名
    private String opRemark;     // 增加人备注
    private String objId;     // 对象ID，如果指定，使用此信息作为类名
    private String refObjId;     // 引用ID，用于对象下子元素

    public ApiType getApiType() {
        return apiType;
    }

    public void setApiType(ApiType apiType) {
        this.apiType = apiType;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getRequire() {
        return require;
    }

    public void setRequire(String require) {
        this.require = require;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getOpTime() {
        return opTime;
    }

    public void setOpTime(String opTime) {
        this.opTime = opTime;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }

    public String getOpRemark() {
        return opRemark;
    }

    public void setOpRemark(String opRemark) {
        this.opRemark = opRemark;
    }

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId;
    }

    public String getRefObjId() {
        return refObjId;
    }

    public void setRefObjId(String refObjId) {
        this.refObjId = refObjId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        ApiDocParamDetail detail = (ApiDocParamDetail) obj;

        return this.apiType == detail.apiType && this.param.equals(detail.param);
    }

    @Override
    public String toString() {
        return "ApiDocParamDetail{" +
                "apiType=" + apiType +
                ", param='" + param + '\'' +
                ", type='" + type + '\'' +
                ", range='" + range + '\'' +
                ", require=" + require +
                ", desc='" + desc + '\'' +
                ", opTime='" + opTime + '\'' +
                ", opName='" + opName + '\'' +
                ", opRemark='" + opRemark + '\'' +
                ", objId='" + objId + '\'' +
                ", refObjId='" + refObjId + '\'' +
                '}';
    }
}
