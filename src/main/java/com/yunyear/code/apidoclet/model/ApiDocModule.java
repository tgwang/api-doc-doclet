package com.yunyear.code.apidoclet.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lenovo on 2017/8/18.
 */
public class ApiDocModule implements Serializable {
    private String name;        // 模块名称
    private String desc;        // 模块描述
    private List<ApiDocCodeDetail> codes;  // 公共返回码
    private List<ApiDocMethodDetail> docs;        // API文档

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ApiDocCodeDetail> getCodes() {
        return codes;
    }

    public void setCodes(List<ApiDocCodeDetail> codes) {
        this.codes = codes;
    }

    public List<ApiDocMethodDetail> getDocs() {
        return docs;
    }

    public void setDocs(List<ApiDocMethodDetail> docs) {
        this.docs = docs;
    }

    @Override
    public String toString() {
        return "ApiDocModule{" +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", codes=" + codes +
                ", docs=" + docs +
                '}';
    }
}
