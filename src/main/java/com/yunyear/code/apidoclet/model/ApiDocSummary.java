package com.yunyear.code.apidoclet.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lenovo on 2017/8/18.
 */
public class ApiDocSummary implements Serializable {
    private String api;
    private String name;
    private String desc;
    private List<String> method;
    private List<String> history;

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getMethod() {
        return method;
    }

    public void setMethod(List<String> method) {
        this.method = method;
    }

    public List<String> getHistory() {
        return history;
    }

    public void setHistory(List<String> history) {
        this.history = history;
    }

    @Override
    public String toString() {
        return "ApiDocSummary{" +
                "api='" + api + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", method=" + method +
                ", history=" + history +
                '}';
    }
}
