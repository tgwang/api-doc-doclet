package com.yunyear.code.apidoclet;

import com.sun.javadoc.RootDoc;
import com.yunyear.code.apidoclet.constant.ConfigConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.tools.java.ClassPath;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Created by lenovo on 2018/5/22.
 */
public class Configuration {
    private static Logger log = LoggerFactory.getLogger(Configuration.class);
    public final static String H_LINE = "-"; // 横线

    private static String workDir = null;
    private static Properties configuration = new Properties();

    public static void start(RootDoc root) throws Exception {
        String configFilename = null;
        String[][] options = root.options();
        for (int i = 0; i < options.length; i++) {
            if ((options[i][0].equals(H_LINE + ConfigConstants.CONF_CONFIG_FILE)) && (options[i].length > 1)) {
                configFilename = options[i][1];
            }
        }
        if (configFilename != null) {
            File configFile = new File(configFilename);
            if (!configFile.exists()) {
                configFile = new File(getWorkDir(), configFilename);
            }
            if (!configFile.exists()) {
                throw new RuntimeException("** Config file not found: " + configFilename + " **");
            }

            configuration.load(new FileInputStream(configFile));
        }

        for (int i = 0; i < options.length; i++) {
            log.debug(">> OPTION " + i + ": " + options[i][0]);
            if (options[i][0].startsWith("-")) {
                String propName = options[i][0];
                propName = propName.substring(1, propName.length());
                String propValue = "";
                if (options[i].length > 1) {
                    propValue = options[i][1];
                }
                log.debug(">> Config property: " + propName + "=" + propValue);
                getConfiguration().setProperty(propName, propValue);
            }
        }
        processConfiguration();
    }

    private static void processConfiguration() {
        if (configuration.getProperty(ConfigConstants.CONF_WORK_DIR) != null) {
            workDir = configuration.getProperty(ConfigConstants.CONF_WORK_DIR);
        }
    }

    public static String getWorkDir() {
        if (workDir == null) {
            workDir = ".";
        }
        if (workDir.endsWith(File.separator)) {
            workDir = workDir.substring(0, workDir.length() - 1);
        }

        log.debug("Work dir: " + workDir);
        return workDir;
    }

    public static Properties getConfiguration() {
        return configuration;
    }

    public static String getProperty(String name) {
        return configuration.getProperty(name);
    }

    public static String getProperty(String name, String defaultValue) {
        return configuration.getProperty(name, defaultValue);
    }

    public static boolean hasProperty(String property) {
        if (getProperty(property) == null) {
            return false;
        }
        return true;
    }

    public static boolean getBooleanConfigValue(String property, boolean defaultValue) {
        String value = getProperty(property);
        boolean result = true;
        if (value != null) {
            if ((value.equalsIgnoreCase("no")) || (value.equalsIgnoreCase("false"))) {
                result = false;
            }
        } else {
            result = defaultValue;
        }
        return result;
    }
}
