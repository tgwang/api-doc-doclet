package com.yunyear.code.apidoclet.taglet;

import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.sun.tools.doclets.formats.html.markup.HtmlTree;
import com.sun.tools.doclets.formats.html.markup.StringContent;
import com.sun.tools.doclets.internal.toolkit.Content;
import com.sun.tools.doclets.internal.toolkit.taglets.BaseTaglet;
import com.sun.tools.doclets.internal.toolkit.taglets.TagletWriter;
import com.yunyear.code.apidoclet.constant.TagConstants;
import com.yunyear.code.apidoclet.utils.JavadocUtil;

/**
 * Created by lenovo on 2018/5/24.
 */
public class ApiParamRespTaglet extends BaseTaglet {
    private static final String NAME = TagConstants.DOC_TAG_PARAM_RESP;
    private static final String HEADER = "API响应信息：";

    @Override
    public boolean inField() {
        return false;
    }

    @Override
    public boolean inConstructor() {
        return false;
    }

    @Override
    public boolean inMethod() {
        return true;
    }

    @Override
    public boolean inOverview() {
        return true;
    }

    @Override
    public boolean inPackage() {
        return false;
    }

    @Override
    public boolean inType() {
        return true;
    }

    @Override
    public boolean isInlineTag() {
        return false;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Content getTagletOutput(Doc doc, TagletWriter tagletWriter) throws IllegalArgumentException {
        StringContent header = new StringContent(HEADER);
        Content content = HtmlTree.DL(HtmlTree.DT(header));

        Tag[] tags = doc.tags(TagConstants.getDocTagFullName(NAME));
        for (Tag tag : tags) {
            content.addContent(HtmlTree.DD(new StringContent(JavadocUtil.getComment(tag))));
        }

        return content;
    }

    //    @Override
//    public String toString(Tag tag) {
//        return "<dt>" + HEADER + "</dt><dd>" + JavadocUtil.getComment(tag) + "</dd>";
//    }
//
//    @Override
//    public String toString(Tag[] tags) {
//        StringBuilder result = new StringBuilder();
//        result.append("<dt>" + HEADER + "</dt>");
//
//        for (Tag tag : tags) {
//            result.append("<dd>" + JavadocUtil.getComment(tag) + "</dd>");
//        }
//
//        return result.toString();
//    }
}
