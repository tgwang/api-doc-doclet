package com.yunyear.code.apidoclet.constant;

/**
 * Created by lenovo on 2018/5/22.
 */
public class TagConstants {
    /**
     * API信息
     */
    public static final String DOC_TAG_API = "api.main";

    /**
     * 请求信息
     */
    public static final String DOC_TAG_PARAM_REQ = "api.param.req";

    /**
     * 响应信息
     */
    public static final String DOC_TAG_PARAM_RESP = "api.param.resp";

    /**
     * 返回码信息
     */
    public static final String DOC_TAG_CODE = "api.code";

    /**
     * 查询tag全称
     *
     * @param tagName
     * @return
     */
    public static String getDocTagFullName(String tagName) {
        return "@" + tagName;
    }
}
