package com.yunyear.code.apidoclet.constant;

/**
 * Created by lenovo on 2018/5/24.
 */
public class ConfigConstants {
    /**
     * 模块名称
     */
    public static final String CONF_OUT_FILE = "out";

    /**
     * 模块名称
     */
    public static final String CONF_MODULE_NAME = "name";

    /**
     * 模块描述
     */
    public static final String CONF_MODULE_DESC = "desc";

    /**
     * 配置文件路径
     */
    public static final String CONF_CONFIG_FILE = "config";

    /**
     * 工作路径
     */
    public static final String CONF_WORK_DIR = "workdir";
}
