package com.yunyear.code.apidoclet;

import com.sun.tools.doclets.internal.toolkit.taglets.Taglet;
import com.yunyear.code.apidoclet.taglet.ApiCodeTaglet;
import com.yunyear.code.apidoclet.taglet.ApiMainTaglet;
import com.yunyear.code.apidoclet.taglet.ApiParamReqTaglet;
import com.yunyear.code.apidoclet.taglet.ApiParamRespTaglet;

import java.util.Map;

/**
 * Created by lenovo on 2018/5/24.
 */
public class ApiTaglet {
    /**
     * 注册 Taglet.
     *
     * @param tagletMap
     */
    public static void register(Map tagletMap) {
        registerTag(tagletMap, new ApiMainTaglet());
        registerTag(tagletMap, new ApiParamReqTaglet());
        registerTag(tagletMap, new ApiParamRespTaglet());
        registerTag(tagletMap, new ApiCodeTaglet());
    }

    /**
     * 注册tag
     *
     * @param tagletMap
     * @param tag
     */
    public static void registerTag(Map tagletMap, Taglet tag) {
        Taglet t = (Taglet) tagletMap.get(tag.getName());
        if (t != null) {
            tagletMap.remove(tag.getName());
        }
        tagletMap.put(tag.getName(), tag);
    }
}
