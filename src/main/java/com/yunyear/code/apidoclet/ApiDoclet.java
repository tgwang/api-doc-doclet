package com.yunyear.code.apidoclet;

import com.google.gson.GsonBuilder;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.Doclet;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.RootDoc;
import com.yunyear.code.apidoclet.constant.ConfigConstants;
import com.yunyear.code.apidoclet.constant.TagConstants;
import com.yunyear.code.apidoclet.model.*;
import com.yunyear.code.apidoclet.utils.ApidocUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by lenovo on 2018/5/22.
 */
public class ApiDoclet extends Doclet {
    private static Logger log = LoggerFactory.getLogger(ApiDoclet.class);

    public static boolean start(RootDoc root) {
        try {
            Configuration.start(root);
            ClassDoc[] classDocs = root.classes();

            ApiDocModule apiDocModule = new ApiDocModule();
            List<ApiDocMethodDetail> methodApiDetails = new ArrayList<>();
            for (ClassDoc classDoc : classDocs) {
                // 查询在类上的api信息
                ApiDocSummary classSummary = ApidocUtil.getFirstApiDocSummary(classDoc);

                if (null == classSummary) {
                    log.debug("类的Javadoc中没有 " + TagConstants.DOC_TAG_API + " 标签，此类不进行扫描，类名：" + classDoc);
                    continue;
                }

                // 查询再类上的返回码信息
                List<ApiDocCodeDetail> classCodes = ApidocUtil.getApiDocCodeDetails(classDoc);

                // 查询再类上的请求参数信息
                List<ApiDocParamDetail> classParamReqs = ApidocUtil.getApiDocParamDetails(classDoc, ApiType.REQ);

                // 查询再类上的请求响应信息
                List<ApiDocParamDetail> classParamResps = ApidocUtil.getApiDocParamDetails(classDoc, ApiType.RESP);

                for (MethodDoc methodDoc : classDoc.methods()) {
                    ApiDocMethodDetail docMethodDetail = new ApiDocMethodDetail();

                    // 查询在方法上的api信息
                    ApiDocSummary methodSummary = ApidocUtil.getFirstApiDocSummary(methodDoc);

                    if (null == methodSummary) {
                        log.debug("类方法的Javadoc中没有 " + TagConstants.DOC_TAG_API + " 标签，此方法不进行扫描，方法名：" + methodDoc);
                        continue;
                    }

                    // 查询再方法上的返回码信息
                    List<ApiDocCodeDetail> methodCodes = ApidocUtil.getApiDocCodeDetails(methodDoc);

                    // 查询再方法上的请求参数信息
                    List<ApiDocParamDetail> methodParamReqs = ApidocUtil.getApiDocParamDetails(methodDoc, ApiType.REQ);

                    // 查询再方法上的请求响应信息
                    List<ApiDocParamDetail> methodParamResps = ApidocUtil.getApiDocParamDetails(methodDoc, ApiType.RESP);

                    // 需要拼接class中的API
                    docMethodDetail.setApi(classSummary.getApi() + "/" + methodSummary.getApi());
                    docMethodDetail.setName(methodSummary.getName());
                    docMethodDetail.setDesc(methodSummary.getDesc());
                    docMethodDetail.setGroup(classSummary.getApi());
                    docMethodDetail.setGroupName(classSummary.getName());

                    List<String> methods = methodSummary.getMethod();
                    methods.removeAll(classSummary.getMethod());
                    methods.addAll(classSummary.getMethod());
                    docMethodDetail.setMethod(methods);

                    List<String> histories = methodSummary.getHistory();
                    histories.removeAll(classSummary.getHistory());
                    histories.addAll(classSummary.getHistory());
                    docMethodDetail.setHistory(histories);

                    methodParamReqs.removeAll(classParamReqs);
                    methodParamReqs.addAll(classParamReqs);
                    docMethodDetail.setReq(methodParamReqs);

                    methodParamResps.removeAll(classParamResps);
                    methodParamResps.addAll(classParamResps);
                    docMethodDetail.setResp(methodParamResps);

                    methodCodes.removeAll(classCodes);
                    methodCodes.addAll(classCodes);
                    docMethodDetail.setCodes(methodCodes);

                    methodApiDetails.add(docMethodDetail);
                }
            }

            if (null != methodApiDetails && methodApiDetails.size() > 0) {
                apiDocModule.setDocs(methodApiDetails);
                apiDocModule.setName(Configuration.getProperty(ConfigConstants.CONF_MODULE_NAME, "API文档"));
                apiDocModule.setDesc(Configuration.getProperty(ConfigConstants.CONF_MODULE_DESC, "API接口协议文档"));
            }

            return writePrettyJson(apiDocModule);
        } catch (Exception e) {
            log.error("异常", e);
            return false;
        }
    }

    /**
     * 需要验证选项长度，如果不识别返回0
     *
     * @param option
     * @return
     */
    public static int optionLength(String option) {
        if (option.equals(Configuration.H_LINE + ConfigConstants.CONF_OUT_FILE)) {
            return 2;
        } else if (option.equals(Configuration.H_LINE + ConfigConstants.CONF_MODULE_NAME)) {
            return 2;
        } else if (option.equals(Configuration.H_LINE + ConfigConstants.CONF_MODULE_DESC)) {
            return 2;
        } else if (option.equals(Configuration.H_LINE + ConfigConstants.CONF_CONFIG_FILE)) {
            return 2;
        } else if (option.equals(Configuration.H_LINE + ConfigConstants.CONF_WORK_DIR)) {
            return 2;
        }

        return 0;
    }

    public static boolean writePrettyJson(ApiDocModule module) {
        String filename = Configuration.getProperty(ConfigConstants.CONF_OUT_FILE, "./api-" + new Date().getTime() + ".json");
        log.debug("Creating ApiDoclet with output file " + filename);

        String workDir = Configuration.getWorkDir();

        // 查看是否是相对目录
        boolean relative = false;
        if ((filename.startsWith(".") || filename.startsWith("..")) && workDir != null) {
            relative = true;
        }

        if (filename.indexOf(File.separator) != -1) {
            String dirName = filename.substring(0, filename.lastIndexOf(File.separator));

            File dir = !relative ? new File(dirName) : new File(workDir, dirName);
            dir.mkdirs();
        }

        File file = !relative ? new File(filename) : new File(workDir, filename);

        log.debug("Filename set to: " + file.getAbsolutePath());

        CharsetEncoder encoder = Charset.forName("UTF-8").newEncoder();
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoder))) {
            String prettyJson = new GsonBuilder().setPrettyPrinting().create().toJson(module);
            writer.write(prettyJson);
            return true;
        } catch (Exception e) {
            log.error("API文档生成异常", e);
            return false;
        }
    }
}