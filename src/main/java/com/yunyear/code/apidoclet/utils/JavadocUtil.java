package com.yunyear.code.apidoclet.utils;

import com.sun.javadoc.Tag;
import com.yunyear.code.apidoclet.model.KeyValuePair;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by lenovo on 2018/5/22.
 */
public class JavadocUtil {
    /**
     * 获取评论信息
     *
     * @param tags
     * @return
     */
    public static String getComment(Tag[] tags) {

        if ((tags == null) || (tags.length == 0)) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < tags.length; i++) {
            buf.append(getComment(tags[i]));
        }
        return buf.toString();
    }

    /**
     * 获取参数值，默认返回控制
     *
     * @param params
     * @param key
     * @return
     */
    public static String getParamValue(List<KeyValuePair<String, String>> params, String key) {
        Optional<KeyValuePair<String, String>> paramOptional = params.stream().filter(param -> key.toLowerCase().equals(param.getKey().trim())).findFirst();

        if (!paramOptional.isPresent()) {
            return "";
        }

        return paramOptional.get().getValue().trim();
    }

    /**
     * 获取参数值，没用返回null
     *
     * @param params
     * @param key
     * @return
     */
    public static String getParamValueDefaultNull(List<KeyValuePair<String, String>> params, String key) {
        Optional<KeyValuePair<String, String>> paramOptional = params.stream().filter(param -> key.toLowerCase().equals(param.getKey().trim())).findFirst();

        if (!paramOptional.isPresent()) {
            return null;
        }

        return paramOptional.get().getValue().trim();
    }

    /**
     * 获取评论信息
     *
     * @param tag
     * @return
     */
    public static String getComment(Tag tag) {
        if (tag == null) {
            return "";
        }
        return tag.text();
    }

    /**
     * 解析并获取评论中的参数信息
     *
     * @param comment
     * @return
     */
    public static List<KeyValuePair<String, String>> getCommentParams(String comment) {
        if (null == comment || "".equals(comment.trim())) {
            return new ArrayList<KeyValuePair<String, String>>();
        }

        // 格式：[a=b] [c=d]
        String[] paramStr = comment.split("\\]\\s+?\\[");

        // 如果只有一个，格式为：[a=b]，为保持后面的处理逻辑一致，需要删掉[
        if(paramStr.length == 1) {
            paramStr[0] = paramStr[0].substring(1, paramStr[0].length());
        }

        List<KeyValuePair<String, String>> params = Arrays.stream(paramStr).map(param -> {
            if (param.startsWith("[")) {
                return param.substring(1, param.length());
            } else if (param.endsWith("]")) {
                return param.substring(0, param.length() - 1);
            } else {
                return param;
            }
        }).map(param -> {
            String[] p = param.split("=", 2);

            if (p.length == 0) {
                return null;
            } else if (p.length == 1) {
                return new KeyValuePair<String, String>(p[0].toLowerCase(), "");
            } else {
                return new KeyValuePair<String, String>(p[0].toLowerCase(), p[1]);
            }
        }).filter(param -> null != param).collect(Collectors.toList());

        return params;
    }

    /**
     * 将参数值转成list形式，使用“,”分隔语句，如果想要保留“,”，请使用"\,"
     *
     * @param value
     * @return
     */
    public static List<String> parseParamValueToList(String value) {
        if (null == value || "".equals(value.trim())) {
            return new ArrayList<>();
        }

        return Arrays.asList(value.split("(?<!\\\\),")).stream().map( val -> val.trim().replace("\\,", ",")).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println(getCommentParams("[a=]]]]]b]  [c=d]  [e=f]"));
        System.out.println(parseParamValueToList("abc\\,dfe,ddd"));

    }
}
