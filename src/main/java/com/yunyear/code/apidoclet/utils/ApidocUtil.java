package com.yunyear.code.apidoclet.utils;

import com.sun.javadoc.Doc;
import com.sun.javadoc.Tag;
import com.yunyear.code.apidoclet.constant.TagConstants;
import com.yunyear.code.apidoclet.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lenovo on 2018/5/23.
 */
public class ApidocUtil {
    /**
     * 获取DocApi参数对象
     *
     * @param comment
     * @param apiType
     * @return
     */
    public static ApiDocParamDetail getApiDocParamDetailFromComment(String comment, ApiType apiType) {
        List<KeyValuePair<String, String>> params = JavadocUtil.getCommentParams(comment);

        ApiDocParamDetail detail = new ApiDocParamDetail();
        detail.setApiType(apiType);

        detail.setParam(JavadocUtil.getParamValue(params, "param"));
        detail.setType(JavadocUtil.getParamValue(params, "type"));
        detail.setRange(JavadocUtil.getParamValue(params, "range"));
        detail.setRequire(JavadocUtil.getParamValue(params, "require"));
        detail.setDesc(JavadocUtil.getParamValueDefaultNull(params, "desc"));
        detail.setOpName(JavadocUtil.getParamValueDefaultNull(params, "opName"));
        detail.setOpTime(JavadocUtil.getParamValueDefaultNull(params, "opTime"));
        detail.setOpRemark(JavadocUtil.getParamValueDefaultNull(params, "opRemark"));
        detail.setObjId(JavadocUtil.getParamValueDefaultNull(params, "objId"));
        detail.setRefObjId(JavadocUtil.getParamValueDefaultNull(params, "refObjId"));
        return detail;
    }

    /**
     * 获取doc下的参数信息
     *
     * @param doc
     * @param apiType
     * @return
     */
    public static List<ApiDocParamDetail> getApiDocParamDetails(Doc doc, ApiType apiType) {
        String tagName = "";
        if (apiType == ApiType.REQ) {
            tagName = TagConstants.DOC_TAG_PARAM_REQ;
        } else if (apiType == ApiType.RESP) {
            tagName = TagConstants.DOC_TAG_PARAM_RESP;
        } else {
            throw new RuntimeException("不被接受的ApiType类型");
        }

        Tag[] tags = doc.tags(TagConstants.getDocTagFullName(tagName));

        if (null == tags || tags.length == 0) {
            return new ArrayList<>();
        }

        return Arrays.stream(tags).map(tag ->
                getApiDocParamDetailFromComment(JavadocUtil.getComment(tag), apiType)
        ).collect(Collectors.toList());
    }

    /**
     * 获取DocApi返回码对象
     *
     * @param comment
     * @return
     */
    public static ApiDocCodeDetail getApiDocCodeDetailFromComment(String comment) {
        List<KeyValuePair<String, String>> params = JavadocUtil.getCommentParams(comment);

        ApiDocCodeDetail detail = new ApiDocCodeDetail();

        detail.setCode(JavadocUtil.getParamValue(params, "code"));
        detail.setMsg(JavadocUtil.getParamValue(params, "msg"));

        return detail;
    }

    /**
     * 获取doc下的参数信息
     *
     * @param doc
     * @return
     */
    public static List<ApiDocCodeDetail> getApiDocCodeDetails(Doc doc) {
        Tag[] tags = doc.tags(TagConstants.getDocTagFullName(TagConstants.DOC_TAG_CODE));

        if (null == tags || tags.length == 0) {
            return new ArrayList<>();
        }

        return Arrays.stream(tags).map(tag ->
                getApiDocCodeDetailFromComment(JavadocUtil.getComment(tag))
        ).collect(Collectors.toList());
    }

    /**
     * 获取DocApiSummary对象
     *
     * @param comment
     * @return
     */
    public static ApiDocSummary getApiDocSummaryFromComment(String comment) {
        List<KeyValuePair<String, String>> params = JavadocUtil.getCommentParams(comment);

        ApiDocSummary summary = new ApiDocSummary();

        summary.setName(JavadocUtil.getParamValue(params, "name"));
        summary.setApi(JavadocUtil.getParamValue(params, "api"));
        summary.setDesc(JavadocUtil.getParamValue(params, "desc"));
        summary.setMethod(JavadocUtil.parseParamValueToList(JavadocUtil.getParamValue(params, "method")));
        summary.setHistory(JavadocUtil.parseParamValueToList(JavadocUtil.getParamValue(params, "history")));

        return summary;
    }

    /**
     * 获取doc下的Summary信息
     *
     * @param doc
     * @return
     */
    public static List<ApiDocSummary> getApiDocSummaries(Doc doc) {
        Tag[] tags = doc.tags(TagConstants.getDocTagFullName(TagConstants.DOC_TAG_API));

        if (null == tags || tags.length == 0) {
            return new ArrayList<>();
        }

        return Arrays.stream(tags).map(tag ->
                getApiDocSummaryFromComment(JavadocUtil.getComment(tag))
        ).collect(Collectors.toList());
    }

    /**
     * 获取第一个Summary信息
     *
     * @param doc
     * @return
     */
    public static ApiDocSummary getFirstApiDocSummary(Doc doc) {
        List<ApiDocSummary> summaries = getApiDocSummaries(doc);

        if (null == summaries || summaries.size() == 0) {
            return null;
        }

        return summaries.get(0);
    }
}
