# JavaDoc 版本API生成器

## doclet介绍

### 新增参数

- `out` 输出文件路径
- `name` 模块名称
- `desc` 模块描述

### 使用样例

```sh
javadoc -encoding utf-8 -doclet com.yunyear.code.apidoclet.ApiDoclet -docletpath D:\data\let\api-doc- doclet-1.0.0-jar-with-dependencies.jar D:\workspace\project\network.test\src\main\java\com\yunyear\tool\log\processtool\javadoc\TagLetDemo.java -out ../json/api-test.json -ae测试API -ec测试的API
```

## taglet介绍

### 使用样例

```sh
javadoc -encoding utf-8 -taglet com.yunyear.code.apidoclet.ApiTaglet -tagletpath D:\data\let\api-doc- doclet-1.0.0-jar-with-dependencies.jar D:\workspace\project\network.test\src\main\java\com\yunyear\tool\log\processtool\javadoc\TagLetDemo.java
```

## 代码书写

- 在类上的tag为本类得公有tag，会作用于类中得每个节点

```java
/**
 * @api.main [api=com.yunyear.tool.log.processtool.javadoc.TagLetDemo]   [name=Taglet测试类] [desc=用于测试Taglet]
 * @api.param.resp [param = code] [type=string] [range=1-16] [require=true] [desc=返回码]
 * @api.param.resp [param = msg] [type=string] [range=1-16] [require=true] [desc=返回描述]
 * @api.code [code=000000] [msg=成功]
 * @api.code [code=999000] [msg=失败]
 */
public class TagLetDemo {
    /**
     * @param args 怎么回事dsadsadsadsadsadsa
     * @api.main [api=main]   [name=main函数] [desc=测试主要入口] [method = RPC]
     * @api.param.req [param=account] [type=string] [range=1-10] [require=true] [desc=账号]
     * @api.param.req [param=password] [type=string] [range=1-16] [require=true] [desc=密码] [objId=User]
     * @api.param.resp [Param = account] [type=string] [range=1-16] [require=true] [desc=用户名] [refObjId=User]
     * @api.code [Code = 000001] [msg = 操作异常]
     */
    public static void maaaa(String[] args) {
    }

}
```